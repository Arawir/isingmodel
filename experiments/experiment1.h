#ifndef EXPERIMENT1
#define EXPERIMENT1

#include <fstream>
#include "../qbox/qbox.h"

double getAverrageSigmaX(double J, double U, uint numberOfNodes, QBox& system)
{
    system.parameter("J")->value() = J;
    system.parameter("U")->value() = U;
    system.newCycle();

    for(uint i=2; i<numberOfNodes; i+=2){
        system.addNode(IncreasingMode::RL);
    }

    for(int i=0; i<10; i+=1){
        system.sweep(IncreasingMode::RL);
    }

    return system.expectedValue("SigmaXSumAB", 0) / double(numberOfNodes);
}

void experiment1(QBox& system)
{
    std::fstream File("../data/experiment1/experiment1.txt", std::ios::out);

    double U = 1.0;

    for(double J=0.01; J<2.0; J+=0.01){
        output << "J/U = " << J/U << std::endl;
        File << J/U << "   "
             << getAverrageSigmaX(J,U,100,system) << "   "
             << std::endl;
    }

    File.close();
}

#endif // EXPERIMENT1

