#include "qbox/parameter.h"

#include "operators/isinghamiltonian.h"
#include "operators/superblockhamiltonian.h"
#include "operators/sigmazonedge.h"
#include "operators/sigmaxsum.h"

#include "experiments/experiment1.h"

int main()
{
    QBox System;

    System.setBlockRank(4);
    System.setNodeRank(2);

    System.addParameter( new Parameter{"J", 1.0} );
    System.addParameter( new Parameter{"U", 1.0} );

    System.addObservable( new SigmaZOnEdge{Block::A, "SigmaZOnEdgeA", &System} );
    System.addObservable( new SigmaZOnEdge{Block::B, "SigmaZOnEdgeB", &System} );

    System.addObservable( new SigmaXSum{Block::A, "SigmaXSumA", &System} );
    System.addObservable( new SigmaXSum{Block::B, "SigmaXSumB", &System} );
    System.addObservable( new SigmaXSum{Block::AB, "SigmaXSumAB", &System} );

    System.setHamiltonianA( new IsingHamiltonian{Block::A, "HA", &System} );
    System.setHamiltonianB( new IsingHamiltonian{Block::B, "HB", &System} );
    System.setSuperblockHamiltonian( new IsingHamiltonianSuperblock{Block::AB, "HAB", &System} );


    experiment1(System);

    return 0;
}

