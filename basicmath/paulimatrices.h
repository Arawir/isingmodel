#ifndef PAULIMATRICES
#define PAULIMATRICES

#include <armadillo>

arma::cx_mat sigmaZ()
{
    arma::cx_mat SigmaZ(2,2);
    SigmaZ(0,0) = 1.0;
    SigmaZ(0,1) = 0.0;
    SigmaZ(1,0) = 0.0;
    SigmaZ(1,1) = -1.0;
    return SigmaZ;
}

arma::cx_mat sigmaX()
{
    arma::cx_mat SigmaX(2,2);
    SigmaX(0,0) = 0.0;
    SigmaX(0,1) = 1.0;
    SigmaX(1,0) = 1.0;
    SigmaX(1,1) = 0.0;
    return SigmaX;
}

#endif // PAULIMATRICES

