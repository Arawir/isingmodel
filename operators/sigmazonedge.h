#ifndef SIGMAZONEDGE
#define SIGMAZONEDGE

#include "../qbox/operator.h"
#include "../basicmath/paulimatrices.h"

class SigmaZOnEdge : public Operator
{
public:
    SigmaZOnEdge(Block block, std::string name, iQBox* box) :
        Operator{block, name, box}
    {

    }

    void increaseRight() override
    {
        this->mat() = (I( this->dim() )^sigmaZ());
    }

    void increaseLeft() override
    {
        this->mat() = (sigmaZ()^I( this->dim() ));
    }

    void newCycle() override
    {
        this->mat() = sigmaZ();
        Operator::newCycle();
    }
};

#endif // SIGMAZONEDGE

