#ifndef SUPERBLOCKHAMILTONIAN
#define SUPERBLOCKHAMILTONIAN

#include "../qbox/operator.h"

class IsingHamiltonianSuperblock : public Operator
{
public:
    IsingHamiltonianSuperblock(Block block, std::string name, iQBox* box) :
        Operator{block, name, box}
    {

    }

    ~IsingHamiltonianSuperblock() = default;

    void recalculate() override
    {
        this->mat()  = ( oII("HA")^I( sqrt(oII("HB").size()) ) );
        this->mat() += ( I( sqrt(oII("HA").size()) )^oII("HB") );
        this->mat() += -pI("J")*( oII("SigmaZOnEdgeA")^oII("SigmaZOnEdgeB") );
    }
};

#endif // SUPERBLOCKHAMILTONIAN

