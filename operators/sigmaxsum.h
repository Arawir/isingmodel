#ifndef SIGMAXSUM
#define SIGMAXSUM

#include "../qbox/operator.h"
#include "../basicmath/paulimatrices.h"

class SigmaXSum : public Operator
{
public:
    SigmaXSum(Block block, std::string name, iQBox* box) :
        Operator{block, name, box}
    {

    }

    void increaseRight() override
    {
        if(block() != Block::AB){
            uint ThisBlockSize = this->dim();
            this->mat() = (this->mat())^I(Box->nodeRank());
            this->mat() += I(ThisBlockSize)^sigmaX();
        }
    }

    void increaseLeft() override
    {
        if(block() != Block::AB){
            uint ThisBlockSize = this->dim();
            this->mat() = I(Box->nodeRank())^(this->mat());
            this->mat() += (sigmaX()^I(ThisBlockSize));
        }
    }

    void recalculate() override
    {
        if(block() == Block::AB){
            arma::cx_mat A = Box->oII("SigmaXSumA");
            arma::cx_mat B = I(sqrt(Box->oII("SigmaXSumB").size()));
            arma::cx_mat C = I(sqrt(Box->oII("SigmaXSumA").size()));
            arma::cx_mat D = Box->oII("SigmaXSumB");

            arma::cx_mat E = A^B;
            arma::cx_mat F = C^D;
            arma::cx_mat G = E+F;


            this->mat() = G;
        }
    }

    void newCycle() override
    {
        this->mat() = sigmaX();
        Operator::newCycle();
    }
};

#endif // SIGMAXSUM

