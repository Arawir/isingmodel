#ifndef ISINGHAMILTONIAN
#define ISINGHAMILTONIAN

#include "../qbox/operator.h"
#include "../basicmath/paulimatrices.h"

class IsingHamiltonian : public Operator
{
public:
    IsingHamiltonian(Block block, std::string name, iQBox* box) :
        Operator{block, name, box}
    {

    }

    ~IsingHamiltonian() = default;

    void increaseRight() override
    {
        this->mat() = ( this->mat()^I(Box->nodeRank()) )
                    - pI("J")*( oI("SigmaZOnEdgeA")^sigmaZ() )
                    - pI("U")*( I(this->dim())^sigmaX() );
    }

    void increaseLeft() override
    {
        this->mat() = ( I(Box->nodeRank())^this->mat() )
                    - pI("J")*( sigmaZ()^oI("SigmaZOnEdgeB") )
                    - pI("U")*( sigmaX()^I(this->dim()) );
    }

    void newCycle() override
    {
        this->mat() = -pI("U")*sigmaX();
        Operator::newCycle();
    }
};

#endif // ISINGHAMILTONIAN

